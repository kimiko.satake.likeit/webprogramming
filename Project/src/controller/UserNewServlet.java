package controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Password;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserNewServlet")
public class UserNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserNewServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// 新規登録のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");



     // リクエストパラメータの入力項目を取得
     		String loginId = request.getParameter("loginId");
     		String password = request.getParameter("password");
     		String password2 = request.getParameter("password2");
     		String name = request.getParameter("name");
     		String birthdate = request.getParameter("birthdate");


     		if(loginId.isEmpty()||password.isEmpty()||password2.isEmpty()||name.isEmpty()||birthdate.isEmpty()){
				request.setAttribute("errMsg", "入力された内容は正しくありません");
	 			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
				dispatcher.forward(request, response);
				return;
     		}
     		else if(!(password2.equals(password)) ){
 			request.setAttribute("errMsg", "入力された内容は正しくありません");
 			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);
			return;
     		}



     		UserDao userDao = new UserDao();

     		if(userDao.idCheck(loginId)!=null ) {
     		request.setAttribute("errMsg", "入力された内容は正しくありません");
 			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/new.jsp");
			dispatcher.forward(request, response);
			return;


     		}
     		Date date = new Date();
    		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		String dateStr = f.format(date);
    		String createdate=dateStr;
    		String updatedate=dateStr;


     		String passmd5 = Password.makehash(password);

     		//int Id = userDao.countId();
     		User user = userDao.regUser(loginId, passmd5,name,birthdate,createdate,updatedate);

    		// セッションにユーザの情報をセット
    		HttpSession session = request.getSession();
    		session.setAttribute("user", user);


    		// ユーザ一覧のサーブレットにリダイレクト
     		response.sendRedirect("UserListServlet");

	}

}
