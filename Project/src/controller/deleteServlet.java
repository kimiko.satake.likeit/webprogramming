package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class detailServlet
 */
@WebServlet("/deleteServlet")
public class deleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public deleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        // リクエストパラメータの入力項目を取得
     	String loginId = request.getParameter("loginId");
     	//System.out.println(loginId);

     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    	UserDao userDao = new UserDao();
    	User user = userDao.showDetail(loginId);

    	request.setAttribute("user",user);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        // リクエストパラメータの入力項目を取得
        String loginid =request.getParameter("loginId");

		UserDao userDao = new UserDao();
		User user = userDao.deleteUser(loginid);

		HttpSession session = request.getSession();
		session.setAttribute("user", user);


		// ユーザ一覧のサーブレットにリダイレクト
 		response.sendRedirect("UserListServlet");




		}




	}

