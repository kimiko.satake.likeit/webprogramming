package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Password;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class updateServlet
 */
@WebServlet("/updateServlet")
public class updateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        // リクエストパラメータの入力項目を取得
     	String loginId = request.getParameter("loginId");
     	//System.out.println(loginId);

     	// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    	UserDao userDao = new UserDao();
    	User user = userDao.showDetail(loginId);

    	request.setAttribute("user",user);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

     // リクエストパラメータの入力項目を取得
 		String password = request.getParameter("password");
 		String password2 = request.getParameter("password2");
 		String name = request.getParameter("name");
 		String birthdate = request.getParameter("birthdate");
 		String loginid =request.getParameter("loginId");


 		if(name.isEmpty()||birthdate.isEmpty()){
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			String loginId = request.getParameter("loginId");
			UserDao userDao = new UserDao();
	    	User user = userDao.showDetail(loginId);

	    	request.setAttribute("user",user);
 			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
 		}
 		else if(!(password2.equals(password)) ){
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			String loginId = request.getParameter("loginId");
			UserDao userDao = new UserDao();
	    	User user = userDao.showDetail(loginId);

	    	request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
 		}

 		UserDao userDao = new UserDao();

 		Date date = new Date();
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = f.format(date);
		String updatedate=dateStr;

		//パスワードを暗号化
 		String passmd5 = Password.makehash(password);

		User user = userDao.updateUser(passmd5,name,birthdate,updatedate,loginid);

		// セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("user", user);


		// ユーザ一覧のサーブレットにリダイレクト
 		response.sendRedirect("UserListServlet");


	}

}
