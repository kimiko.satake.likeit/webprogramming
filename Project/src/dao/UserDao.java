package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;




public class UserDao {


    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public User idCheck(String loginId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

    }

    public int countId() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();
        int count = 0;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();


            String sql = "SELECT MAX(id) FROM user";


            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);


            count=rs.getInt("id");



        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
        return count+1;
    }



    public User regUser (String loginId,String password ,String name,String  birthdate,String createdate,String updatedate){

    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // INSERT文を準備
        String sql = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,?,?)";

        //
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        pStmt.setString(2, name);
        pStmt.setString(3, birthdate);
        pStmt.setString(4, password);
        pStmt.setString(5, createdate);
        pStmt.setString(6, updatedate);

        pStmt.executeUpdate();

    }catch (SQLException e) {
        e.printStackTrace();

    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
    }
	return null;

  }


    /**
     * 全てのユーザ情報を取得する
     * @return
	**/

    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // 管理者以外を取得
            String sql = "SELECT * FROM user WHERE not name='管理者'" ;

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }




//詳細取得
    public User showDetail(String loginId) {
    	Connection conn = null;
    try {
        // データベースへ接続
    		conn = DBManager.getConnection();

    		// SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);

            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            String birthdateDate = rs.getString("birth_date");
            String createdateData =rs.getString("create_date");
            String updateData =rs.getString("update_date");

            return new User(loginIdData, nameData,birthdateDate,createdateData,updateData);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    }


//更新

public User updateUser(String password ,String name,String  birthdate,String updatedate,String loginid){

    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // INSERT文を準備
        String sql = "UPDATE user SET name=?,birth_date=?,password=?,update_date=? WHERE login_id=?";

        //
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1,name);
        pStmt.setString(2, birthdate);
        pStmt.setString(3, password);
        pStmt.setString(4, updatedate);
        pStmt.setString(5, loginid);


        pStmt.executeUpdate();

    }catch (SQLException e) {
        e.printStackTrace();

    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
    }
	return null;

  }

//削除

public User deleteUser(String loginid){

  Connection conn = null;
  try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // INSERT文を準備
      String sql = "DELETE FROM user WHERE login_id=?";

      //
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginid);
      pStmt.executeUpdate();

  }catch (SQLException e) {
      e.printStackTrace();

  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();

          }
      }
  }
	return null;

}

//検索機能

public List<User> findInfo(String name,String loginId,String dateStart,String dateEnd){
	Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備

        String sql = "SELECT * FROM user WHERE not name='管理者'";
        if(!(name.equals(""))) {
        	sql += " and name LIKE '%"+name+"%'";}
        if(!(loginId.equals(""))) {
        	sql +=" and login_id ='"+loginId+"'" ;}
        if(!((dateStart.equals("")))){
        	sql +=" and '"+dateStart +"' <= birth_date";
        }
        if(!((dateEnd.equals("")))){
        	sql +=" and '" + dateEnd+ "' >= birth_date";
        }
        //System.out.println(sql);



         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        ResultSet rs = pStmt.executeQuery();

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int id = rs.getInt("id");
            String loginid = rs.getString("login_id");
            String username = rs.getString("name");
            String birthDate = rs.getString("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginid, username, birthDate, password, createDate, updateDate);

            userList.add(user);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;
}
}




