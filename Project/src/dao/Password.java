package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class Password {
	//ハッシュを生成したい元の文字列
	 public static String makehash(String password) {
	String source = "password";
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	String result = null;
	//ハッシュ生成処理
	try {
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		result = DatatypeConverter.printHexBinary(bytes);

	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
	return result;
	 }

}
