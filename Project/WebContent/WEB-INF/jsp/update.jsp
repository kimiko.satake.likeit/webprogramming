<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
	<meta name="Author" content=""/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

 <div class="header">
    <p>${userInfo.name}さん
     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
    </p>
</div>

<div class="container">
<h1>ユーザ情報更新</h1>

 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<form action="updateServlet" method="post">
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-6 col-form-label">ログインid</label>
    <div class="col-sm-6">
      <input type="text" readonly class="form-control-plaintext" name="loginId" id="loginid" value="${user.loginId}">
    </div>
  </div>
 <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">パスワード</label>
    <div class="col-sm-6">
      <input type="password" class="form-control" name="password" id="inputPassword" value="${user.password}">
    </div>

  </div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">パスワード(確認)
    </label>
    <div class="col-sm-6">
      <input type="password" class="form-control" name="password2" id="inputPassword"  value="">
    </div>

  </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">ユーザ名</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="name" id="inputPassword" value="${user.name}">
    </div>

  </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">生年月日</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="birthdate" id="inputBirthdate" value="${user.birthDate}">
    </div>

  </div>
    <div class="submit-center">
    <button type="submit" class="btn btn-secondary">更新</button>
    </div>
</form>
</div>





<a href="UserListServlet">戻る</a>

</body>
</html>
