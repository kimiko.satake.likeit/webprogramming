<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
	<meta name="Author" content=""/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
    <p>${user.name}さん
     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
    </p>
</div>

    <div class="container">
    <h1 class="mt-4">ユーザ情報詳細参照</h1>


  <table class="table table-borderless">

  <tbody>
    <tr>
      <td scope="row">ログインid</td>
      <td>${user.loginId}</td>

    </tr>
    <tr>
      <td scope="row">ユーザ名</td>
      <td>${user.name}</td>

    </tr>

    <tr>
      <td scope="row">生年月日</td>
      <td>${user.birthDate}</td>

    </tr>

    <tr>
      <td scope="row">登録日時</td>
      <td>${user.createDate}</td>

    </tr>

    <tr>
      <td scope="row">更新日時</td>
      <td>${user.updateDate}</td>

    </tr>

  </tbody>
</table>



<a href="UserListServlet">戻る</a>
    </div>


</body>
</html>