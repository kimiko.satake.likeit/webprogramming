<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
	<meta name="Author" content=""/>
	<link rel="stylesheet"  href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>



    <h1 m-10>ログイン画面</h1>

    <div class="container">
	 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

   <form class="form-signin" action="LoginServlet" method="post">
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">ログインID</label>
    <div class="col-sm-6">
      <input type="text" class="form-control"name="loginId" id="inputLoginId" >
    </div>

  </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">パスワード</label>
    <div class="col-sm-6">
      <input type="password" class="form-control" name="password" id="inputPassword" >
    </div>

    </div>
        <div class="submit-center">
        <button class="btn btn-secondary">ログイン</button>
        </div>
    </form>

    </div>




</body>
</html>
