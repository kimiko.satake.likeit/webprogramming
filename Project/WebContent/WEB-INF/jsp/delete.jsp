<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
	<meta name="Author" content=""/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="header">
    <p>${user.name}
    <a href="login.html">ログアウト</a>
    </p>
</div>

<div class="container">
<h1>ユーザ削除確認</h1>
<form action="deleteServlet" method="post">
<p>ログインID:
<input type="text" readonly class="form-control-plaintext" name="loginId" value="${user.loginId}">
を本当に削除してよろしいでしょうか？</p>

<a href="UserListServlet" class="btn btn-secondary">キャンセル</a>

 <button type="submit" class="btn btn-secondary">OK</button>
</form>

</div>

</body>
</html>
