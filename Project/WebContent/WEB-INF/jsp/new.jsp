<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="UTF-8">
    <title>Untitled Document</title>
	<meta name="Author" content=""/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<div class="header">
    <p>${userInfo.name} さん
     <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
    </p>
</div>


<div class="container">
    <h1>ユーザ新規登録</h1>

 <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


    <form class="form-new" action="UserNewServlet" method="post">
<div class="inputarea">
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">ログインID</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="loginId" id="inputLoginId">
    </div>

  </div>
 <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">パスワード</label>
    <div class="col-sm-6">
      <input type="password" class="form-control" name="password" id="inputPassword" >
    </div>

  </div>
<div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">パスワード(確認)
    </label>
    <div class="col-sm-6">
      <input type="password" class="form-control" name="password2" id="inputPassword2">
    </div>

  </div>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">ユーザ名</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="name" id="inputname">
    </div>
    </div>

   <div class="form-group row">
    <label for="inputPassword" class="col-sm-6 col-form-label">生年月日</label>
    <div class="col-sm-6">
      <input type="Date" class="form-control" name="birthdate" id="inputBirthdate">
    </div>







</div>

  </div>
        <div class="submit-center">
            <button type="submit" class="btn btn-secondary">新規登録</button>
        </div>
</form>
</div>


<p><a href="UserListServlet">戻る</a></p>


</body>
</html>
